"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
        
    """
    # Initalize new array
    length = len(x)
    abs_acc = np.zeros(length)
    # Calculate absolute acceleration at each timestep and store in new array
    for i in range(length):
        abs_acc[i] = np.sqrt(x[i]**2 + y[i]**2 + z[i]**2)
    # Return results
    return abs_acc


def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Create interpolation points
    new_times = np.arange(0,20,20/len(time))
    # Interpolate values based on created points and data
    new_data = np.interp(new_times,time,data)
    # Return results
    return (new_times,new_data)


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    # Input signals average is not zero -> subtract average from input
    average = np.mean(x)
    x = x - average
    # Calculate FFT amplitude
    FFT_amplitude = np.fft.fft(x)
    FFT_amplitude = np.abs(FFT_amplitude)
    # Calculate Frequency
    N = len(x)
    n = np.arange(N)
    sampling_rate = len(time)/20
    T = N/sampling_rate
    FFT_frequency = n/T
    # Return results
    return (FFT_amplitude,FFT_frequency)

    